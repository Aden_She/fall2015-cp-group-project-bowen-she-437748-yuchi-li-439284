<?php
header("Content-type: application/jason");

require 'database.php';

$stmt=$mysqli->prepare("select username, post_link, post_date, post_title, post_description,img_link,img_count from users u inner join posts p on u.usr_id = p.user_id inner join post_details d on p.post_id = d.post_id;");
if(!$stmt){
    echo json_encode(array(
        "success"=>false,
        "message"=>"Qurey Prep Failed1: %s\n", $mysqli->error
    ));
    exit;
}

$stmt->execute();

$result = $stmt->get_result();



while($row = $result->fetch_assoc()){
        
         $data[] = array(
            "success" =>true,
            "imgLink" =>htmlspecialchars( $row["img_link"] ),
            "imgCount" =>htmlspecialchars( $row["img_count"] ),
            "postTitle" =>htmlspecialchars( $row["post_title"] ),
            "username" =>htmlspecialchars( $row["username"] ),
            "rentDescription" =>htmlspecialchars( $row["post_description"] ),
            "postLink" =>htmlspecialchars( $row["post_link"] ),
            "date" =>htmlspecialchars( $row["post_date"] ),
           );
        
    }
    echo json_encode(array("success"=>true, "data"=>$data));
   // echo jason_encode($arr);
  exit;
?>