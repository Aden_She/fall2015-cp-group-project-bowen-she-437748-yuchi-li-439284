$(document).ready(function() {
   $("#register_btn").click(function(){
        var usernameval = $("#username").val();
        var passwordval = $("#password").val();
        var passwordReval = $("#passwordRe").val();
          if (usernameval == "" || passwordval == "" || passwordReval == "") {
            return;
        }
        if (passwordval != passwordReval) {
            alert("Please enter the same password!");
            return;
        }
        var arr = {"username":usernameval, "password":passwordval, "passwordRe": passwordReval};
        $.ajax( {
            type: "POST",
            url: "registerCheck_ajax.php",
            //url: "test.php",
            data: arr,
            success: function(response) {
                var jsonData = $.parseJSON(response);
               
               if (jsonData.success) {
                    window.location.replace('mainPage.html');
                }
                  else {
                    alert(jsonData.message);
                  }
                  
                },
            error: function(exception){alert('Exception:' + exception);}});
        return false;
   }); 
});