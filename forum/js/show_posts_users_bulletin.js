$(document).ready(function(){
    $('a[href="#Posts"]').on('shown.bs.tab', function(e){
    //TO DO: show posts from table posts with tag, title, poster, time
    $("#photoWall").empty();

     $.ajax(
            {
                type: "POST",
                url: "showPosts_ajax.php",
                success:function(response) {
                   var jsonData = $.parseJSON(response); //parse JSON
                  
                  if (jsonData.success) {
                    var events= jsonData.data;
                    var num_events = events.length;
                  }
              
                   //TO DO: parse jason Objects from servers to produce a column with tag, title, user of the post, time
              for(var i =0; i <num_events; i++) {
                         $("#photoWall").append("<div  id = wallPhoto" + i+"></div>");
                        $wallPhoto = $('#photoWall').find('#wallPhoto'+i);
                        $wallPhoto.append("<div class='box'><div class = 'oldContent'><img src ='' height = 300 width = 300 ><p></p></div>");
                        $wallPhoto.find('.box').append("<div class = 'newContent'></div></div>");
                        $wallPhoto.find("img").attr("src",events[i].imgLink);
                        $wallPhoto.find("p").html("<h3>"+events[i].postTitle+"</h3>&nbsp;"+events[i].date+" &nbsp;"+events[i].username);
                        $wallPhoto.find(".newContent").append("<p id = rentDescrption>"+events[i].rentDescription+"</p");
                        $wallPhoto.find(".newContent").append("<button class='btn btn-success' id = gotoPost href=" + events[i].postLink+">take a look</div>");
                        box_transform();
               // return false;
              }
                   
                  
                },
                error: function(exception){alert("No");alert('Exception:' + exception);}});
           
        return false;
    });
});

$('a[href="#Users"]').on('shown.bs.tab', function (e) {
   

});
$('a[href="#Bulletin"]').on('shown.bs.tab', function (e) {
   
});

//animation of post

function box_transform(){
           $('.box').hover(function() {
            
            $this = $(this),
            $oC = $this.find('.oldContent'),
            $nC = $this.find('.newContent');
          
            
            $oC.stop(true, true).fadeOut('fast');
            
            $this.stop(true, true).animate({
                width: '+=100',
                height: '+=000',
               // top: '+=100'
            }, function() {
                $nC.fadeIn('fast');
            });
        
        }, function() {
            
            $nC.stop(true, true).fadeOut('fast');
   
            
            $this.stop(true, true).animate({
                width: '-=100',
                height: '-=000',
               // top: '-=100'
            }, function() {
                $oC.fadeIn('fast');
             
            });
       
        });
          
};
        
   
