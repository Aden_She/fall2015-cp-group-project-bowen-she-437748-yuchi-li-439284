<?php
header("Content-type: application/jason");

$username = $_POST['username'];
$password=$_POST['password'];
$passwordRe=$_POST['passwordRe'];

if($password!=$passwordRe){
    echo json_encode(array(
        "success"=>false,
        "message"=>"Passwords don't match."
    ));
    exit;
}


require 'database.php';

$stmt=$mysqli->prepare("select username from users");
if(!$stmt){
    echo json_encode(array(
        "success"=>false,
        "message"=>"Qurey Prep Failed1: %s\n", $mysqli->error
    ));
    exit;
} 

$stmt->execute();

$stmt->bind_result($reg_username);

while($stmt->fetch()){
    if(htmlspecialchars($reg_username)==htmlspecialchars($_POST['username'])){
        echo json_encode(array(
            "success"=>false,
            "message"=>"Username already exist."
        ));
        exit;
    }
}

if(isset($_POST['username'])){
    $username=$_POST['username'];
}

if(isset($_POST['password'])){
    $crypt_password=crypt($password);
}

$stmt=$mysqli->prepare("insert into users (usr_id,username,password) values (' ',?,?)");
if(!$stmt){
    echo json_encode(array(
        "success"=>false,
        "message"=>"Query Prep Failed2: %s\n"
    ));
    exit;
}
else{

    $stmt->bind_param('ss',$username,$crypt_password);
    $stmt->execute();
    $stmt->close();
    
    session_start();
    $_SESSION['username'] = $username;
    $_SESSION['token'] = substr(md5(rand()), 0, 10);
    
    echo json_encode(array("success"=>true));
    exit;}


?>