<?php
header("Content-type: application/jason");


$username = $_POST['username'];
$password = $_POST['password'];

require 'database.php';

$stmt = $mysqli->prepare("select count(*), usr_id, password from users where username=?");
                         
if (!$stmt) {
    echo json_encode(array(
        "success" => false,
        "message" => "Query prep Failed."
    ));
}

$stmt->bind_param('s',$username);

$stmt->execute();


$stmt->bind_result($cnt,$user_id,$pwd_hash);

$stmt->fetch();
$stmt->close();


if($cnt==0) {
    echo json_encode(array(
        "success"=>false,
        "message"=>"Your account does not exist! Why don't you join us?"
    ));
    exit;
}


if ($cnt==1 && crypt($password, $pwd_hash) == $pwd_hash) {
    session_start();
    $_SESSION['username'] = $username;
    $_SESSION['token'] = substr(md5(rand()), 0, 10);
    
    echo json_encode(array(
        "success" => true
    ));
    exit;
}
    else {
        echo json_encode(array(
        "success" => false,
        "message" => "Your password is not correct. Please re-enter it again!"
        ));
        exit;}
?>